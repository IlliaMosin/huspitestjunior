const datesWrapper = document.getElementById("dates"),
    month = document.getElementById("month"),
    arrowPrev = document.getElementById("prev-month"),
    arrowNext = document.getElementById("next-month"),
    months = ["january", "february", "march", "april", " may", "june", "july", "august", "septemer", "october", "november", "december"],
    now = new Date(),
    curDay = now.getDate();

var curMonth = now.getMonth(),
    curYear = now.getFullYear(),
    items;

const cal = document.querySelector(".calendar");

const addEvent = function (day, month, year, eventTime, smallDescription, fullDescription) {
    let item = items[day - 1];
    if (item === undefined) {
        return;
    } else {
        if (curMonth + 1 === month && curYear === year) {
            let fragment = document.createDocumentFragment(),
                box = document.createElement("a"),
                spansWrapper = document.createElement("div"),
                link = document.createElement("a"),
                dateSpan = document.createElement("span"),
                timeSpan = document.createElement("span"),
                eventsMonths = months[month - 1].substr(0, 3),
                activeEvent = document.querySelector('div');
            activeEvent.addEventListener('click', function () {
                prompt('enter your plan');
            });
            link.textContent = smallDescription;
            link.className = "event-description";
            link.setAttribute("href", "#");
            dateSpan.className = "box-span box-span-date";
            dateSpan.textContent = "".concat(day, " ").concat(eventsMonths, " ").concat(year);
            timeSpan.className = "box-span box-span-time";
            timeSpan.textContent = eventTime;
            spansWrapper.className = "span-wrapper";
            spansWrapper.append(dateSpan, timeSpan);
            box.className = "box";
            box.setAttribute("href", "#");
            box.appendChild(spansWrapper);
            fragment.append(link, box);
            item.className = "date event";
            item.prepend(fragment);
        }
    }
};


const removeOldEvents = function () {
    let events = document.querySelectorAll(".date.event");
    if (events.length > 0) {
        let links = document.querySelectorAll(".event-description"),
            box = document.querySelectorAll(".box");
        for (i = 0; i < events.length; i++) {
            let e = events[i],
                l = links[i],
                b = box[i];
            b.outerHTML = "";
            l.outerHTML = "";
            e.className = "date";
        }
    }
};

const addEvents = function () {
    items = document.querySelectorAll(".date");
    addEvent(25, 6, 2019,  "can i do this work?");
};


const getDaysInMonth = function () {
    return new Date(curYear, curMonth + 1, 0).getDate();
};


const monthStartDay = function () {
    let firstDay = new Date(curYear, curMonth, 1),
        startDay = firstDay.getDay();
    if (startDay == 0) startDay = 7;
    return startDay;
};

const createBlanks = function() {
    let startDay = monthStartDay() - 1,
        fragment = document.createDocumentFragment(),
        itemsLength = document.querySelectorAll(".blank").length;
    if (itemsLength !== startDay) {
        let diff = startDay - itemsLength;
        if (diff > 0) {
            for (let _i = itemsLength; _i < startDay; _i++) {
                let element = document.createElement("div");
                element.className = "blank";
                fragment.appendChild(element);
            }
            datesWrapper.prepend(fragment);
        } else {
            for (let _i2 = -diff; _i2 > 0; _i2--) {
                datesWrapper.removeChild(datesWrapper.firstChild);
            }
        }
    }
};


const createDates = function() {
    let numberOfDays = getDaysInMonth(),
        fragment = document.createDocumentFragment(),
        itemsLength = document.querySelectorAll(".date").length;
    if (itemsLength !== numberOfDays) {
        let diff = numberOfDays - itemsLength;

        if (diff > 0) {
            for (let _i3 = itemsLength; _i3 < numberOfDays; _i3++) {
                let element = document.createElement("div"),
                    span = document.createElement("span");
                span.textContent = _i3 + 1;
                span.className = "month-day";
                element.className = "date";
                element.appendChild(span);
                fragment.appendChild(element);
            }
            datesWrapper.appendChild(fragment);
        } else {
            for (let _i4 = -diff; _i4 > 0; _i4--) {
                datesWrapper.removeChild(datesWrapper.lastChild);
            }
        }
    }
};

const setTodayActive = function() {
    let ourMonth = now.getMonth(),
        ourYear = now.getFullYear(),
        spans = document.querySelectorAll(".month-day");

    if (curMonth === ourMonth && curYear === ourYear) {
        spans[curDay - 1].className = "month-day current-date";
    } else if (spans[curDay - 1] && spans[curDay - 1].classList.contains("current-date")) {
        spans[curDay - 1].className = "month-day";
    }
};

const createContent = function() {
    let currentMonth = months[curMonth];
    month.textContent = currentMonth + " " + curYear;
    createBlanks();
    createDates();
    setTodayActive();
};

const moveMonth = function(start, end) {
    console.time();

    if (curMonth === start) {
        curMonth = end;
        start < end ? curYear-- : curYear++;
    } else if (start || end) {
        start < end ? curMonth-- : curMonth++;
    }

    createContent();
    removeOldEvents();
    addEvents();
    console.timeEnd();
};

moveMonth();
arrowPrev.addEventListener("click", function () {
    return moveMonth(0, 11);
});
arrowNext.addEventListener("click", function () {
    return moveMonth(11, 0);
});